const bcrypt = require('bcrypt');
const check = require('../validation/CheckValidation') 
const conn = require('../../config/db')
const moment = require('moment'); 
// User login
var nodemailer = require('nodemailer');
const authLogin = async (req, res) => {
    let message = null
    let statusCode = 400
    let error = {}
    let data = {}
  
    try {
        const errors = check.resultsValidator(req)
        if (errors.length > 0) {
            return res.status(400).json({
                method: req.method,
                status: res.statusCode,
                error: errors
            })
        } else {
            const {imei_id,device_id} = req.body 
            
            // Check requeted user is exist or not
            let sql = `SELECT * FROM users WHERE LOWER(device_id)= ? AND LOWER(imei_id)= ? limit ?`;
            let user = await conn.query(sql, [device_id.toLowerCase(),imei_id.toLowerCase(), 1]);
            const usersRows = (JSON.parse(JSON.stringify(user))[0]); 
            const last_login = moment().format("YYYY-MM-DD HH:mm:ss");
            if (user.length > 0) {  
                const formData1 = {
                    is_login:1,
                    last_login:last_login
                }
                let sql1 = "UPDATE users Set ? WHERE user_id= ?"
                await conn.query(sql1, [formData1,usersRows.user_id])
                statusCode = 200
                message = 'Login success'
                //check bonos
                var t = new Date();
                var dd = String(t.getDate()).padStart(2, '0');
                var mm = String(t.getMonth() + 1).padStart(2, '0'); //January is 0!
                var yyyy = t.getFullYear();
                
                const today = `${yyyy}-${mm}-${dd}`; 
                let sql = `SELECT * FROM daily_bonus_table WHERE DATE(currentdate) = ? AND user_id=? limit ?`;
                let bonoss = await conn.query(sql, [today,usersRows.user_id, 1]);
                if(bonoss.length>0){

                }else{
                    /**
                     * DAILY LOGIn
                     */
                    let sql1 = `SELECT * FROM daily_bonus_master_table WHERE is_active = ? limit ?`;
                    let bonus_master = await conn.query(sql1, [1, 1]);
                    
                    const formData = {
                        user_id         : usersRows.user_id, 
                        daily_bonus_code : bonus_master[0].daily_bonos_master_id,
                        currenttime     : moment(Date.now()).format('HH:mm:ss'),
                        currentdate     : today,
                        login_date_time : `${today} ${moment(Date.now()).format('HH:mm:ss')}`
                    };
                
                    let sql2  = `INSERT INTO daily_bonus_table set ?`;
                    await conn.query(sql2, formData)
                    
                    /**
                     * WEEKLY LOGIn
                     */
                    
                    const day1 = t.getDay(); 
                    if(day1 == 5)
                    {
                        let sql = `SELECT * FROM weekly_bonus_table WHERE DATE(currentdate) = ? AND user_id=? limit ?`;
                        let bonoss = await conn.query(sql, [today,usersRows.user_id, 1]);
                        if(!(bonoss.length>0))
                        {
                            let sql1w = `SELECT * FROM weekly_bonus_master_table WHERE is_active = ? limit ?`;
                            let bonus_master = await conn.query(sql1w, [1, 1]);
                            const formData1 = {
                                user_id         : usersRows.user_id, 
                                weekly_bonus_code: bonus_master[0].weekly_bonos_master_id,
                                currenttime     : moment(Date.now()).format('HH:mm:ss'),
                                currentdate     : today,
                                login_date_time : `${today} ${moment(Date.now()).format('HH:mm:ss')}`
                            }; 
                            let sql3  = `INSERT INTO weekly_bonus_table set ?`;
                            await conn.query(sql3, formData1)
                        }
                        
                    }
                }

                let dsql = `SELECT daily_bonus_table.*,daily_bonus_master_table.coins FROM daily_bonus_table LEFT JOIN daily_bonus_master_table ON 
                daily_bonus_table.daily_bonus_code=daily_bonus_master_table.daily_bonos_master_id  
                WHERE daily_bonus_table.is_used = 0 AND daily_bonus_table.is_active = 1 AND daily_bonus_table.user_id =? limit ?`;
                let daily = await conn.query(dsql, [usersRows.user_id,30]);

                let wsql = `SELECT weekly_bonus_table.*,weekly_bonus_master_table.coins FROM weekly_bonus_table LEFT JOIN weekly_bonus_master_table ON 
                weekly_bonus_table.weekly_bono_code=weekly_bonus_master_table.weekly_bonos_master_id  
                WHERE weekly_bonus_table.is_used = 0 AND weekly_bonus_table.is_active = 1 AND weekly_bonus_table.user_id =? limit ?`;
                let weekly = await conn.query(wsql, [usersRows.user_id,5]);
                bonus = {
                    weekly:weekly,
                    daily : daily
                }
                
                data = { 
                    username:`Guest00${usersRows.user_id}`,
                    bonus 
                }
               
            } else {
                const formData = { 
                    imei_id,
                    device_id,
                    is_login:1,
                    last_login
                };
                 
                const sql1S  = `INSERT INTO users set ?`;
                const userss = await conn.query(sql1S, formData)
                 
                // const user_id = userss.insertId
                statusCode = 200
                message = 'Login success'
                //check bonos
                var t = new Date();
                var dd = String(t.getDate()).padStart(2, '0');
                var mm = String(t.getMonth() + 1).padStart(2, '0'); //January is 0!
                var yyyy = t.getFullYear();
                
                const today = `${yyyy}-${mm}-${dd}`; 
                let sql = `SELECT * FROM daily_bonus_table WHERE DATE(currentdate) = ? AND user_id=? limit ?`;
                let bonoss = await conn.query(sql, [today,user_id, 1]);
                if(bonoss.length>0){

                }else{
                    /**
                     * DAILY LOGIn
                     */
                    let sql1 = `SELECT * FROM daily_bonus_master_table WHERE is_active = ? limit ?`;
                    let bonus_master = await conn.query(sql1, [1, 1]);
                    
                    const formData = {
                        user_id         :  user_id, 
                        daily_bonus_code : bonus_master[0].daily_bonos_master_id,
                        currenttime     : moment(Date.now()).format('HH:mm:ss'),
                        currentdate     : today,
                        login_date_time : `${today} ${moment(Date.now()).format('HH:mm:ss')}`
                    };
                
                    let sql2  = `INSERT INTO daily_bonus_table set ?`;
                    await conn.query(sql2, formData)
                    
                    /**
                     * WEEKLY LOGIn
                     */
                    
                    const day1 = t.getDay(); 
                    if(day1 == 5)
                    {
                        let sql = `SELECT * FROM weekly_bonus_table WHERE DATE(currentdate) = ? AND user_id=? limit ?`;
                        let bonoss = await conn.query(sql, [today,user_id, 1]);
                        if(!(bonoss.length>0))
                        {
                            let sql1w = `SELECT * FROM weekly_bonus_master_table WHERE is_active = ? limit ?`;
                            let bonus_master = await conn.query(sql1w, [1, 1]);
                            const formData1 = {
                                user_id         : user_id, 
                                weekly_bonus_code: bonus_master[0].weekly_bonos_master_id,
                                currenttime     : moment(Date.now()).format('HH:mm:ss'),
                                currentdate     : today,
                                login_date_time : `${today} ${moment(Date.now()).format('HH:mm:ss')}`
                            }; 
                            let sql3  = `INSERT INTO weekly_bonus_table set ?`;
                            await conn.query(sql3, formData1)
                        }
                        
                    }
                }

                let dsql1 = `SELECT daily_bonus_table.*,daily_bonus_master_table.coins FROM daily_bonus_table LEFT JOIN daily_bonus_master_table ON 
                daily_bonus_table.daily_bonus_code=daily_bonus_master_table.daily_bonos_master_id  
                WHERE daily_bonus_table.is_used = 0 AND daily_bonus_table.is_active = 1 AND daily_bonus_table.user_id =? limit ?`;
                let daily = await conn.query(dsql1, [user_id,30]);

                let wsql1 = `SELECT weekly_bonus_table.*,weekly_bonus_master_table.coins FROM weekly_bonus_table LEFT JOIN weekly_bonus_master_table ON 
                weekly_bonus_table.weekly_bono_code=weekly_bonus_master_table.weekly_bonos_master_id  
                WHERE weekly_bonus_table.is_used = 0 AND weekly_bonus_table.is_active = 1 AND weekly_bonus_table.user_id =? limit ?`;
                let weekly = await conn.query(wsql1, [user_id,5]);
                bonus = {
                    weekly:weekly,
                    daily : daily
                }
                
            
                data = { 
                    username:`Guest00${user_id}`,
                    bonus 
                }
                   
 
            } 
            const responseData = {
                status: statusCode,
                message,
                 data,
                errors: error
            }
        res.send(responseData)
    }
    } catch (error) {
        res.send({ Err: error })
    }
}
 
 
module.exports = {
    authLogin 
}