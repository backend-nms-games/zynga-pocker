const conn   = require('../../config/db') 
const moment = require('moment');
const path   = require('path');
const check  = require('../validation/CheckValidation')
const {checkUser} = require('../socket/Users')
var pan = require('validate-india').pan;
let message = null
let status  = 400
let response={}
let errors={}
let data = {};
// Add money function

const uploadProfilePic = async (req, res) => { 
    try {
        // const errors = check.resultsValidator(req)
        // if (errors.length > 0) {
        //     return res.status(400).json({
        //         method: req.method,
        //         status: res.statusCode,
        //         error: errors
        //     })
        // }d
        let sql = `SELECT * FROM users WHERE user_id= ? limit ?`;
        let user = await conn.query(sql, [req.body.user_id, 1]);
        console.log(user,'as')
        if(!user.length>0)res.send({status:404,message:"user not found"})
        console.log(req.body)
        if (!req.files || Object.keys(req.files).length == 0) {
            res.status(400).send({ status: statusCode, message: 'No file are uploaded' })
        }
        prfoileFile = req.files.avatar
        let reqPath = path.join(__dirname, '../../public')
        const imagUrl= `Avatars/${prfoileFile.name}`
        uploadPath = `${reqPath}/${imagUrl}` 
        const profile = await prfoileFile.mv(uploadPath)

        let sql1 = "UPDATE users Set avatar= ? WHERE user_id= ?"
        const users = await conn.query(sql1, [imagUrl, req.body.user_id]);
        let statusCode = 404
        if (users) {
            statusCode = 200
            message = 'Image uploaded success'
        } else {
            statusCode = 500
            message = 'Unable to upload'
        }

        const responseData = {
            status: statusCode,
            message,
            errors: {}
        }
        res.send(responseData)
    } catch (error) {
        res.send('error')
    }

}
const retrieveProfilePic = async (req, res) => {
    console.log('121221212121212',pan.isValid('DPVPM8916J'))
    try {
        console.log('re',req.params.id)
        let sql = `SELECT * FROM users WHERE user_id= ? limit ?`;
        let user = await conn.query(sql, [req.params.id, 1]);
        const usersRows = (JSON.parse(JSON.stringify(user))[0]); 

        if(usersRows.avatar ==null) usersRows.avatar = 'Avatars/default.png';
        const responseData = {
            status: 200,
            message:'Success',
            avatarLink:`${req.protocol}://${req.headers.host}/${usersRows.avatar}`,
            errors: 'error'
        }  
        res.send(responseData) 
    } catch (e) {
        res.status(404).send()
    }

}

const getUsers = async (req, res) => {  
    // con
    try { 
        let sql = `SELECT * FROM users`;
        let user = await conn.query(sql);
        if(user.length>0){
            status = 200;
            message ='Success'
            const usersRows = (JSON.parse(JSON.stringify(user))); 
            data = usersRows
        }else{
            status = 404
            message = 'Users not found'
            data = {};
        } 
        const responseData = {
            status: status,
            message:message, 
            data: data
        } 
        res.send(responseData) 
    } catch (e) {
        res.status(404).send('ERR')
    }

}

const getProfiles = async (req, res) => {  
    // con
    try { 
        const { user_id } = req.body 
        let sql = `SELECT * FROM users WHERE user_id= ? limit ?`;
        let user = await conn.query(sql, [user_id, 1]);
        if(user.length>0){
            status = 200;
            message ='My Profile'
            const usersRows = (JSON.parse(JSON.stringify(user))[0]); 
            console.log(usersRows)
            data ={
                user_id     : usersRows.user_id,
                username    : usersRows.username,
                first_name  : usersRows.first_name,
                last_name   : usersRows.last_name,
                email       : usersRows.email,
                phone       : usersRows.phone,
                gender      : usersRows.gender==1?"M":"F",
                state       : usersRows.state,
                city        : usersRows.city,
                postal_code : usersRows.postal_code,
                google_id   : usersRows.google_id,
                cash_balance: usersRows.cash_balance,
                bonus_amount: usersRows.bonus_amount,
                vip_points  : usersRows.vip_points,
                last_login  : usersRows.last_login,
                coin_balance: usersRows.coin_balance,
                date_of_birth: usersRows.date_of_birth,
                winning_balance: usersRows.winning_balance,
                monthly_vip_points: usersRows.monthly_vip_points,
                yearly_vip_points: usersRows.yearly_vip_points
            } 
        }else{
            status = 404
            message = 'Invalid user Id'
            data = {};
        } 
        const responseData = {
            status: status,
            message:message, 
            data: data
        } 
        res.send(responseData) 
    } catch (e) {
        res.status(404).send('ERR')
    }

}
module.exports = { 
    uploadProfilePic,
    retrieveProfilePic,
    getUsers,
    getProfiles
}