const router = require('express').Router()
//Import auth controller
const Games = require('../controllers/GameControllers') 
//import validation
const check = require('../validation/CheckValidation')

router.get('/category',Games.getGameCategory)
router.get('/bid_amounts',Games.getGameBidAmounts)
router.get('/:categoryID',Games.getRanksByID)
// router.get('/status/:status',Games.getGamesByStatus)
module.exports = router
 