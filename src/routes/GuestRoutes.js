const router = require('express').Router()
// import auth controller
const GuestController = require('../controllers/GuestController')

// Import auth middleware
// const Auth = require('../middleware/Auth')

//import validation
const check = require('../validation/CheckValidation')

// route list
// router.post('/signUp',check.registerValidator(),GuestController.authSignUp)
router.post('/login',check.guestValidatr(),GuestController.authLogin)
// router.post('/forgotPassword',check.forgotPasswordValidator(),AuthController.forgotPassword)
// router.post('/resetPassword',check.resetPasswordValidator(),AuthController.resetPassword)
// router.post('/forgotPassword',check.forgotPasswordValidator(),AuthController.forgotPassword)


module.exports = router